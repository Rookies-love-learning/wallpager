package com.myfootball.wallpaper.network

import android.util.Log
import android.widget.Toast
import androidx.core.util.Consumer
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.myfootball.wallpaper.MyApplication
import com.lzy.okgo.callback.StringCallback
import com.lzy.okgo.model.Response

open class OkSimpleCallback<T>(



    private val clazz: Class<T>,

    private val owner: LifecycleOwner,
    private val successCallback: Consumer<T> = Consumer { }, private val successToast: Boolean = true,
    private val exceptionCallback: Consumer<T> = Consumer { }, private val exceptionToast: Boolean = true,
    private val errorCallback: Consumer<Pair<Throwable, Response<String>?>> = Consumer { }, private val errorToast: Boolean = true,
    private val asyncTaskParseJson: Boolean = false
) : StringCallback() {
    override fun onSuccess(response: Response<String>?) {
        if (owner.lifecycle.currentState != Lifecycle.State.DESTROYED) {
            kotlin.runCatching {
//                JSON.parseObject(response?.body(), object : TypeReference<ApiModel<T>>(clazz) {})
                MyGson.gson.fromJson(response?.body(), clazz)
            }.onSuccess {
                runOnUI {
                    successCallback.accept(it)
                }
            }.onFailure {
                runOnUI {
                    errorCallback.accept(it to response)
                    if (errorToast) {
                        Toast.makeText(MyApplication.getInstances(), "API服务器接口返回异常 : $it , API接口 : ${response?.rawResponse}", Toast.LENGTH_LONG)
                            .show()
                    }
                    Log.e("ERROR", "API", it)
                }
            }
        }
    }


    override fun onError(response: Response<String>?) {
        super.onError(response)
        kotlin.runCatching {

        }.onFailure {
            Log.e("ERROR", "onError", it)
        }
    }

}