package com.myfootball.wallpaper.network;

import androidx.annotation.Keep;

@Keep
public class ApiModel<T> {

    public int code = 0;
    public String msg = "";
    public T data;

}
