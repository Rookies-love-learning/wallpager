package com.myfootball.wallpaper.network

import androidx.core.util.Consumer
import androidx.lifecycle.LifecycleOwner
import com.myfootball.wallpaper.bean.SettingBean
import com.lzy.okgo.OkGo
import com.lzy.okgo.model.Response

object MyOkApi {

    fun getShellJson(owner: LifecycleOwner, consumer: Consumer<SettingBean>, errorCallback: Consumer<Pair<Throwable, Response<String>?>>) {
        OkGo.get<String>("https://my-json-server.typicode.com/ndrewlim0789/teen_shel/db")
            .execute(object : OkSimpleCallback<SettingBean>(SettingBean::class.java, owner, consumer, errorCallback = errorCallback) {})
    }

}