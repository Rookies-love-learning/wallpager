package com.myfootball.wallpaper.dialog;

@FunctionalInterface
public interface IAction {
    /**
     * Runs the action and optionally
     */
    void run();
}
