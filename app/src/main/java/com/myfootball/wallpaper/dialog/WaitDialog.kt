package com.myfootball.wallpaper.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.myfootball.wallpaper.R
import com.myfootball.wallpaper.kts.setDialogFragmentTranslucent

class WaitDialog(var dismissAction : IAction = IAction {  }) : BaseDialogFragment() {
    private var tipStr = "加载中,请稍候..."

    override fun onStart() {
        super.onStart()
        setDialogFragmentTranslucent(dialog)
    }

    override fun setBackgroundColor(view: View) {

    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        dismissAction.run()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.nv_dialog_loading, container)
    }

    override fun onFragmentCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
    }

    fun setTip(_tipStr : String){
        tipStr = _tipStr
    }

    private fun initView(view: View){
        isCancelable = true
        val tip = view.findViewById<TextView>(R.id.tip)
        tip.text = tipStr
    }

}