package com.myfootball.wallpaper.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.hjm.bottomtabbar.BottomTabBar;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.fragment.TabFourFragment;
import com.myfootball.wallpaper.fragment.TabOneFragment;
import com.myfootball.wallpaper.fragment.TabTwoFragment;

public class MainActivity extends AppCompatActivity {

    private static int REQUEST_PERMISSION_CODE = 1;

    private BottomTabBar botttom_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        checkPermission();

        botttom_bar = findViewById(R.id.bottom_bar);
        botttom_bar.init(getSupportFragmentManager())
                .setImgSize(30, 30)
                .setFontSize(10)
                .setTabPadding(4, 4, 4)
                .setChangeColor(getResources().getColor(R.color.red1), getResources().getColor(R.color.bg1))
                //添加fragment
                .addTabItem("首页", R.drawable.tabone_pressed, R.drawable.tabone_normal, TabOneFragment.class)
                .addTabItem("排序", R.drawable.tabtwo_pressed, R.drawable.tabtwo_normal, TabTwoFragment.class)
//                .addTabItem("分类", R.drawable.tabthree_pressed, R.drawable.tabthree_normal, TabThreeFragment.class)
                .addTabItem("我的", R.drawable.tabfour_pressed, R.drawable.tabfour_normal, TabFourFragment.class)
        ;
    }

    private void checkPermission() {
        //检查权限（NEED_PERMISSION）是否被授权 PackageManager.PERMISSION_GRANTED表示同意授权
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //用户已经拒绝过一次，再次弹出权限申请对话框需要给用户一个解释
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                    .WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "请开通相关权限，否则无法正常使用本应用！", Toast.LENGTH_SHORT).show();
            }
            //申请权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CODE);
        } else {
//            Toast.makeText(this, "授权成功！", Toast.LENGTH_SHORT).show();
            Log.e("aaaaa", "checkPermission: 已经授权！");

        }
    }
}