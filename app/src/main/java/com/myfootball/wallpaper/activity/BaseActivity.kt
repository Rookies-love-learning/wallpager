package com.myfootball.wallpaper.activity

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.transition.Slide
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewbinding.ViewBinding
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.gyf.immersionbar.ImmersionBar
import com.myfootball.wallpaper.util.MyStatusBarUtil
import com.myfootball.wallpaper.R
import com.myfootball.wallpaper.base.LoadingDialogManager

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {
    protected var mToolbar: Toolbar? = null
    protected var mTextTitle: TextView? = null
    protected var mImageViewOne: ImageView? = null
    protected var mImageViewTwo: ImageView? = null
    lateinit var binding: VB
    private var mLoadingDialogManager: LoadingDialogManager? = null

    @SuppressLint("RtlHardcoded")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        val slide = Slide()
        slide.duration = 250
        slide.slideEdge = Gravity.RIGHT
        window.enterTransition = slide
        setWindow()
        binding = getViewBinding() as VB
        setContentView(binding.root)
        MyStatusBarUtil.setStatusBarLightMode(this)
        initSystemBarTint()
        mToolbar = findViewById(R.id.toolbar)
        if (mToolbar != null) {
            initToolBar()
        }
        _initView(savedInstanceState)
        _initData()
    }

    /**
     * return ActivityMainBinding.inflate(layoutInflater)
     */
    protected abstract fun getViewBinding(): ViewBinding
    protected abstract fun _initView(savedInstanceState: Bundle?)
    protected abstract fun _initData()
    protected fun setWindow() {}
    protected fun initToolBar() {
        setSupportActionBar(mToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val drawable = ContextCompat.getDrawable(this, R.mipmap.chevron_left_square)!!
        val state = drawable.constantState
        val drawable1 = DrawableCompat.wrap((state?.newDrawable() ?: drawable)).mutate()
        drawable1.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this, R.color.toolbar_black_color))
        mToolbar?.navigationIcon = drawable
        mTextTitle = findViewById(R.id.tv_title)
        mImageViewOne = findViewById(R.id.iv_left_one)
        mImageViewTwo = findViewById(R.id.iv_left_two)
    }

    fun setTitle(title: String?) {
        if (mTextTitle != null) {
            mTextTitle?.text = title
        }
    }

    override fun setTitle(titleId: Int) {
        if (mTextTitle != null) {
            mTextTitle?.text = getString(titleId)
        }
    }

    fun showLoading(text: String? = "Loading...") {
        if (mLoadingDialogManager == null) {
            mLoadingDialogManager = LoadingDialogManager(this)
        }
        mLoadingDialogManager!!.showLoading(text)
    }

    fun dismissLoading() {
        if (mLoadingDialogManager == null) {
            mLoadingDialogManager = LoadingDialogManager(this)
        }
        mLoadingDialogManager!!.dissLoading()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initSystemBarTint() {
        val window = window
        if (translucentStatusBar()) {
            // 设置状态栏全透明
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
            ImmersionBar.with(this)
                .statusBarDarkFont(false, 0.2f)
                .navigationBarColor(R.color.browser_actions_divider_color)
                .init()
        }
    }

    open fun translucentStatusBar(): Boolean {
        return false
    }



}