package com.myfootball.wallpaper.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.myfootball.wallpaper.R;

public class AboutActivity extends AppCompatActivity {

    private LinearLayout lin_back_about;
    private TextView tv_version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        tv_version = (TextView) findViewById(R.id.tv_version);
        lin_back_about = (LinearLayout) findViewById(R.id.lin_back_about);

        lin_back_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AboutActivity.this, "已经是最新版本了！", Toast.LENGTH_SHORT).show();
            }
        });
    }
}