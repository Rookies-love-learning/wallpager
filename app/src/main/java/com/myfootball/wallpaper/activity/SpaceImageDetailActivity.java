package com.myfootball.wallpaper.activity;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.myfootball.wallpaper.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class SpaceImageDetailActivity extends AppCompatActivity {


    private PhotoView photoview;
    private String img_url;
    private ImageView img_save;
    private ImageView img_set;
    private ImageView img_share;
    private LinearLayout lin_save;
    private LinearLayout lin_set;
    private LinearLayout lin_share;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_image_detail);

        initData();
        initUI();
        setLister();

        Glide.with(this).asBitmap().load(img_url).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                bitmap = resource;
                Log.e("bitmap","------succeed");
            }
        });
    }

    private void initUI() {
        photoview = (PhotoView) findViewById(R.id.photoview);
        Glide.with(this).load(img_url).into(photoview);

        img_save = (ImageView) findViewById(R.id.img_save);
        img_set = (ImageView) findViewById(R.id.img_set);
        img_share = (ImageView) findViewById(R.id.img_share);
        lin_save = (LinearLayout) findViewById(R.id.lin_save);
        lin_set = (LinearLayout) findViewById(R.id.lin_set);
        lin_share = (LinearLayout) findViewById(R.id.lin_share);
    }

    private void initData() {
        Intent intent = getIntent();
        img_url = intent.getStringExtra("img_url");
    }

    private void setLister() {
        photoview.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float x, float y) {
                finish();
            }
        });

        lin_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveImage(bitmap);
                }catch (Exception e){}
                Toast.makeText(SpaceImageDetailActivity.this, "保存成功", Toast.LENGTH_LONG).show();
            }
        });

        lin_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //设置图片为壁纸
                WallpaperManager manager = WallpaperManager.getInstance(SpaceImageDetailActivity.this);
                try {
                    manager.setBitmap(bitmap);
                    Toast.makeText(SpaceImageDetailActivity.this, "壁纸设置成功，请在桌面上查看", Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(SpaceImageDetailActivity.this, "壁纸设置成失败", Toast.LENGTH_LONG).show();
                }

            }
        });

        lin_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    shareImage(bitmap);
                } catch (Exception e) { }
                Toast.makeText(SpaceImageDetailActivity.this, "分享成功！", Toast.LENGTH_LONG).show();
            }
        });

    }

    /**
     * getImage for url
     */
    public Bitmap GetImageInputStream(String imageurl) {
        URL url;
        HttpURLConnection connection = null;
        Bitmap bitmap = null;
        try {
            url = new URL(imageurl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(6000);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            InputStream inputStream = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * shareImage
     */
    private void shareImage(Bitmap bitmap) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "IMG" + Calendar.getInstance().getTime(), null));
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "share"));
    }


    /**
     * saveImage
     */
    private void saveImage(Bitmap bitmap)  {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;
        String filename;//声明文件名
        //以保存时间为文件名
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat ("yyyyMMddHHmmss");
        filename =  sdf.format(date);
        File file = new File(extStorageDirectory, filename+".JPEG");//创建文件，第一个参数为路径，第二个参数为文件名
        try {
            outStream = new FileOutputStream(file);//创建输入流
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.close();
            /**
             * 三行可以实现相册更新
             * 这个广播的目的就是更新图库，发了这个广播进入相册就可以找到你保存的图片了！
             */
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(file);intent.setData(uri);
            sendBroadcast(intent);
            Log.e("save","------succeed");
//            Toast.makeText(SpaceImageDetailActivity.this,"saved", Toast.LENGTH_SHORT).show();
        } catch(Exception e) {
            Log.e("save","------exception:" + e);
//            Toast.makeText(SpaceImageDetailActivity.this, "exception:" + e, Toast.LENGTH_SHORT).show();
        }
    }

}