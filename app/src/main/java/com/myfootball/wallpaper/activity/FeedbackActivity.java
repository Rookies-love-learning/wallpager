package com.myfootball.wallpaper.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.myfootball.wallpaper.R;

public class FeedbackActivity extends AppCompatActivity {

    private Dialog progressDialog;
    private LinearLayout lin_back_feedback;
    private EditText ed_input;
    private TextView tv_tijiao;
    private String s_text;

    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        lin_back_feedback = (LinearLayout) findViewById(R.id.lin_back_feedback);
        ed_input = (EditText) findViewById(R.id.ed_input);
        tv_tijiao = (TextView) findViewById(R.id.tv_tijiao);

        lin_back_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_tijiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_text = ed_input.getText().toString().trim();
                if("".equals(s_text)){
                    Toast.makeText(FeedbackActivity.this, "请输入您的意见", Toast.LENGTH_SHORT).show();
                }else {
                    showDialog("感谢您的宝贵意见！");
                }
            }
        });
    }

    private void showDialog(String text) {
        progressDialog = new Dialog(this, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.dialog);
        progressDialog.setCancelable(true);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView msg = (TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
        msg.setText("loading...");
        progressDialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(progressDialog != null){
                    progressDialog.dismiss();
                    Toast.makeText(FeedbackActivity.this, text, Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        }, 800);
    }
}