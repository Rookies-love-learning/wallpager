package com.myfootball.wallpaper.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.view.MyGridView;

import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    private LinearLayout lin_back_category;
    private TextView tv_category;
    private TextView tv_name;
    private String name;
    private String introduce;
    private List<String> img_list;
    private MyGridView gridView;
    private GridAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        initData();
        initUI();
        setLister();

     }

    private void initUI() {
        lin_back_category = (LinearLayout) findViewById(R.id.lin_back_category);
        tv_category = (TextView) findViewById(R.id.tv_category);
        tv_category.setText(name);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_name.setText(introduce);

        gridView = (MyGridView) findViewById(R.id.grid_three);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridView.setFocusable(false);
        myAdapter = new GridAdapter(this, img_list);
        gridView.setAdapter(myAdapter);
    }

    private void initData() {
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        introduce = intent.getStringExtra("introduce");
        img_list = intent.getStringArrayListExtra("img_list");
    }

    private void setLister() {
        lin_back_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CategoryActivity.this, SpaceImageDetailActivity.class);
                intent.putExtra("img_url", img_list.get(position));
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    public class GridAdapter extends BaseAdapter {

        private Context mContext;
        private List<String> data;

        public GridAdapter(Context mContext, List<String> data){
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            View view;
            if(convertView == null){
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(mContext).inflate(R.layout.item_list_sort, null);
                view.setTag(viewHolder);
            }else{
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.img = (ImageView) view.findViewById(R.id.item_img_one);
            Glide.with(CategoryActivity.this).load(data.get(position)).into(viewHolder.img);
            return view;
        }

        class ViewHolder{
            ImageView img;
        }
    }
}