package com.myfootball.wallpaper.kts

import android.app.Dialog
import android.view.ViewGroup
import android.view.WindowManager
import com.myfootball.wallpaper.R

//半透明
fun setDialogFragmentTranslucent(dialog : Dialog?){
    dialog?.window?.let {
        it.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        it.setBackgroundDrawableResource(R.color.translucent)
        it.decorView.setPadding(0, 0, 0, 0)
        val attr = it.attributes
        attr.dimAmount = 0.0f
        it.attributes = attr
        it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }
}

//全透明
fun setDialogFragmentTransparent(dialog: Dialog?) {
    dialog?.window?.let {
        it.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        it.setBackgroundDrawableResource(R.color.transparent)
        it.decorView.setPadding(0, 0, 0, 0)
        val attr = it.attributes
        attr.dimAmount = 0.0f
        it.attributes = attr
        it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }
}


