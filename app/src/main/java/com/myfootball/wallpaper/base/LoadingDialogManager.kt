package com.myfootball.wallpaper.base

import androidx.appcompat.app.AppCompatActivity
import com.myfootball.wallpaper.dialog.WaitDialog

class LoadingDialogManager(private val context: AppCompatActivity) {

    private var waitDialog = WaitDialog()

    @JvmOverloads
    fun showLoading(text: String? = "Loading") {
        waitDialog.show(context.supportFragmentManager, text)
    }

    fun dissLoading() {
        waitDialog.dismiss();
    }

    /**
     * 延迟消失
     * @param time 毫秒
     */
    fun dissLoading(time: Int) {
//        waitDialog.dismiss()
    }
}