package com.myfootball.wallpaper.view;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.myfootball.wallpaper.R;
import com.just.agentweb.IWebLayout;

public class WebLayout implements IWebLayout {

    private Activity mActivity;
    private LinearLayout myLayout;
    private WebView mWebView = null;

    public WebLayout(Activity activity) {
        this.mActivity = activity;
        myLayout = (LinearLayout) LayoutInflater.from(activity).inflate(R.layout.mywebview, null);
        mWebView = (WebView) myLayout.findViewById(R.id.webView);
    }

    @NonNull
    @Override
    public ViewGroup getLayout() {
        return myLayout;
    }

    @Nullable
    @Override
    public WebView getWebView() {
        return mWebView;
    }

}
