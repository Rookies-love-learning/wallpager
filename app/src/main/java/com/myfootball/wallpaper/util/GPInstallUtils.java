package com.myfootball.wallpaper.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * google play install utils
 */
public class GPInstallUtils {


    public void setGpInstallInfo(Context context , String json){
        //步骤1：创建一个SharedPreferences对象
        SharedPreferences sharedPreferences= context.getSharedPreferences("GpInstallInfo",Context.MODE_PRIVATE);
        //步骤2： 实例化SharedPreferences.Editor对象
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //步骤3：将获取过来的值放入文件
        editor.putString("json", json);
        //步骤4：提交
        editor.commit();
    }

    public String getGpInstallInfo(Context context){
        SharedPreferences sharedPreferences=context.getSharedPreferences("GpInstallInfo", Context .MODE_PRIVATE);
        String json=sharedPreferences.getString("json","");
        return json ;
    }

}
