package com.myfootball.wallpaper.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.bean.LikeBean;

import java.util.List;

/**
 * author: Wang
 * createDate: 2022/3/7 11:10
 */
public class HomeAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private List<List<String>> list_img;
    public HomeAdapter(int layoutResId,List<List<String>> list_img) {
        super(layoutResId);
        this.list_img = list_img;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String string) {

        Glide.with(getContext()).load(list_img.get(i).get(0)).into((ImageView) baseViewHolder.getView(R.id.pop_img));

    }
}
