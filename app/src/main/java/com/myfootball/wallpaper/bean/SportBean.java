package com.myfootball.wallpaper.bean;

import java.util.List;

public class SportBean {

    private List<DataBean> data;

    public static class DataBean {
        private String urlImage;

        public String getUrlImage() {
            return urlImage;
        }

        public void setUrlImage(String urlImage) {
            this.urlImage = urlImage;
        }
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }
}
