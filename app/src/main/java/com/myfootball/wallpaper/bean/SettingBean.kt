package com.myfootball.wallpaper.bean

import androidx.annotation.Keep

@Keep
class SettingBean {



    var posts: List<Post> = listOf()
    var profile: Profile = Profile()
    var settingList: List<Setting> = listOf()
}

@Keep
class Post {

    var id: Int = 0
    var title: String = ""
}

@Keep
class Profile {
    var name: String = ""
}

@Keep
class Setting {
    var appId: Int = 0
    var name: String = ""
    var `open`: Boolean = false
    var url: String = ""
}