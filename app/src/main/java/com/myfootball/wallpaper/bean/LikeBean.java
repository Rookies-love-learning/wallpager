package com.myfootball.wallpaper.bean;

/**
 * author: Wang
 * createDate: 2022/3/7 13:41
 */
public class LikeBean {
    private int imgR;

    public LikeBean(int imgR) {
        this.imgR = imgR;
    }

    public int getImgR() {
        return imgR;
    }
}
