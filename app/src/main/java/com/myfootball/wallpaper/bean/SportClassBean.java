package com.myfootball.wallpaper.bean;

import java.util.List;

public class SportClassBean {

    private List<DataListBean> data;

    public static class DataListBean {

        private String name;
        private String introduce;
        private List<ImgBean> img_list;

        public static class ImgBean {
            private String urlImage;

            public String getUrlImage() {
                return urlImage;
            }

            public void setUrlImage(String urlImage) {
                this.urlImage = urlImage;
            }
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public List<ImgBean> getImg_list() {
            return img_list;
        }

        public void setImg_list(List<ImgBean> img_list) {
            this.img_list = img_list;
        }
    }

    public List<DataListBean> getData() {
        return data;
    }

    public void setData(List<DataListBean> data) {
        this.data = data;
    }
}
