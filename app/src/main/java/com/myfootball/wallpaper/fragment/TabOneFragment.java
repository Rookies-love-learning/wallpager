package com.myfootball.wallpaper.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.activity.CategoryActivity;
import com.myfootball.wallpaper.activity.SpaceImageDetailActivity;
import com.myfootball.wallpaper.adapter.HomeAdapter;
import com.myfootball.wallpaper.bean.SportBean;
import com.myfootball.wallpaper.bean.SportClassBean;
import com.myfootball.wallpaper.util.LocalJsonResolutionUtils;
import com.myfootball.wallpaper.view.MyGridView;
import com.myfootball.wallpaper.view.MyListView;
import com.myfootball.wallpaper.view.RoundImageView;

import java.util.ArrayList;
import java.util.List;

public class TabOneFragment extends Fragment {

    private View inflate;
    private String fileName1 = "sport_class.json";
    private String sportClassJson;
    private List<String> list_name = new ArrayList<>();
    private List<String> list_introduce = new ArrayList<>();
    private List<List<String>> list_img = new ArrayList<>();
    private MyGridView listview;
    private MyAdapter myAdapter;

    private HomeAdapter homeAdapter;
    private RecyclerView home_recycleview;



    private String fileName2 = "sport_part.json";
    private String imageJson;
    private List<String> list_sport = new ArrayList<>();
    private MyListView list_part;
    private ListAdapter listAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(inflate == null){
            inflate = inflater.inflate(R.layout.fragment_tab_one, container, false);
            initData();
            initUI();
//            setLister();
        }
        return inflate;
    }

    private void initUI() {
//        listview = (MyGridView) inflate.findViewById(R.id.list_one);
//        listview.setSelector(new ColorDrawable(Color.TRANSPARENT));
//        myAdapter = new MyAdapter(getContext(), list_name, list_img);
//        listview.setAdapter(myAdapter);
//
//        list_part = (MyListView) inflate.findViewById(R.id.list_part);
//        list_part.setSelector(new ColorDrawable(Color.TRANSPARENT));
//        listAdapter = new ListAdapter(getContext(), list_sport);
//        list_part.setAdapter(listAdapter);

        home_recycleview =  inflate.findViewById(R.id.home_recycleview);
        GridLayoutManager gridLayoutManager =new GridLayoutManager(getContext(),2);
        gridLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        homeAdapter = new HomeAdapter(R.layout.pop_item,list_img);
        home_recycleview.setAdapter(homeAdapter);
        home_recycleview.setLayoutManager(gridLayoutManager);
        homeAdapter.setNewData(list_sport);

    }

    private void initData() {
        sportClassJson = LocalJsonResolutionUtils.getJson(getContext(), fileName1);
        SportClassBean sportClassBean = LocalJsonResolutionUtils.JsonToObject(sportClassJson, SportClassBean.class);
        for (int i = 0; i < sportClassBean.getData().size(); i++){
            list_name.add(sportClassBean.getData().get(i).getName());
        }
        for (int i = 0; i < sportClassBean.getData().size(); i++){
            list_introduce.add(sportClassBean.getData().get(i).getIntroduce());
        }
        for (int i = 0; i < sportClassBean.getData().size(); i++){
            List<String> img = new ArrayList<>();
            for (int j = 0; j < sportClassBean.getData().get(i).getImg_list().size(); j++){
                img.add(sportClassBean.getData().get(i).getImg_list().get(j).getUrlImage());
            }
            list_img.add(img);
        }

        imageJson = LocalJsonResolutionUtils.getJson(getContext(), fileName2);
        SportBean sportBean = LocalJsonResolutionUtils.JsonToObject(imageJson, SportBean.class);
        for (int i = 0; i < sportBean.getData().size(); i++){
            list_sport.add(sportBean.getData().get(i).getUrlImage());
        }

    }

    private void setLister() {
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), CategoryActivity.class);
                intent.putExtra("introduce", list_introduce.get(position));
                intent.putExtra("name", list_name.get(position));
                intent.putStringArrayListExtra("img_list", (ArrayList<String>) list_img.get(position));
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        list_part.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), SpaceImageDetailActivity.class);
                intent.putExtra("img_url", list_sport.get(position));
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    public class MyAdapter extends BaseAdapter {
        private Context mContext;
        private List<String> l_name;
        private List<List<String>> l_img;

        public MyAdapter(Context mContext, List<String> l_name, List<List<String>> l_img){
            this.mContext = mContext;
            this.l_name = l_name;
            this.l_img = l_img;

        }

        @Override
        public int getCount() {
            return l_name.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            View view;
            if(convertView == null){
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(mContext).inflate(R.layout.item_list_sportclass, null);
                view.setTag(viewHolder);
            }else{
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.img = (RoundImageView) view.findViewById(R.id.item_img_three);
            viewHolder.tv = (TextView) view.findViewById(R.id.item_tv_three);
            Glide.with(getContext()).load(l_img.get(i).get(0)).into(viewHolder.img);
            viewHolder.tv.setText(l_name.get(i));
            return view;
        }

        class ViewHolder{
            RoundImageView img;
            TextView tv;
        }
    }

    public class ListAdapter extends BaseAdapter{

        private Context mContext;
        private List<String> data;

        public ListAdapter(Context mContext, List<String> data){
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            View view;
            if(convertView == null){
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(mContext).inflate(R.layout.item_list_sport, null);
                view.setTag(viewHolder);
            }else{
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.img = (ImageView) view.findViewById(R.id.item_img_one);
            Glide.with(getContext()).load(data.get(position)).into(viewHolder.img);
            return view;
        }

        class ViewHolder{
            ImageView img;
        }
    }

}