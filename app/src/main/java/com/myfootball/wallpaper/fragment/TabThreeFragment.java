package com.myfootball.wallpaper.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.activity.CategoryActivity;
import com.myfootball.wallpaper.bean.SportClassBean;
import com.myfootball.wallpaper.util.LocalJsonResolutionUtils;
import com.myfootball.wallpaper.view.RoundImageView;

import java.util.ArrayList;
import java.util.List;

public class TabThreeFragment extends Fragment {

    private View inflate;
    private String fileName = "sport_class.json";
    private String sportClassJson;
    private List<String> list_name = new ArrayList<>();
    private List<String> list_introduce = new ArrayList<>();
    private List<List<String>> list_img = new ArrayList<>();
    private ListView listview;
    private MyAdapter myAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(inflate == null){
            inflate = inflater.inflate(R.layout.fragment_tab_three, container, false);
            initData();
            initUI();
            setLister();
        }
        return inflate;
    }

    private void initUI() {
        listview = (ListView) inflate.findViewById(R.id.list_three);
        listview.setSelector(new ColorDrawable(Color.TRANSPARENT));
        myAdapter = new MyAdapter(getContext(), list_name, list_img);
        listview.setAdapter(myAdapter);
    }

    private void initData() {
        sportClassJson = LocalJsonResolutionUtils.getJson(getContext(), fileName);
        SportClassBean sportClassBean = LocalJsonResolutionUtils.JsonToObject(sportClassJson, SportClassBean.class);
        for (int i = 0; i < sportClassBean.getData().size(); i++){
            list_name.add(sportClassBean.getData().get(i).getName());
        }
        for (int i = 0; i < sportClassBean.getData().size(); i++){
            list_introduce.add(sportClassBean.getData().get(i).getIntroduce());
        }
        for (int i = 0; i < sportClassBean.getData().size(); i++){
            List<String> img = new ArrayList<>();
            for (int j = 0; j < sportClassBean.getData().get(i).getImg_list().size(); j++){
                img.add(sportClassBean.getData().get(i).getImg_list().get(j).getUrlImage());
            }
            list_img.add(img);
        }

        /*
        for (int i = 0; i < list_img.size(); i++){
            Log.e("----------" + i,"---------");
            for (int j = 0; j < list_img.get(i).size(); j++){
                Log.e("list_img=" + i + "====" + j,"" + list_img.get(i).get(j));
            }
            Log.e("----------" + i,"---------");
        }
         */
    }

    private void setLister() {
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), CategoryActivity.class);
                intent.putExtra("introduce", list_introduce.get(position));
                intent.putExtra("name", list_name.get(position));
                intent.putStringArrayListExtra("img_list", (ArrayList<String>) list_img.get(position));
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });
    }

    public class MyAdapter extends BaseAdapter {
        private Context mContext;
        private List<String> l_name;
        private List<List<String>> l_img;

        public MyAdapter(Context mContext, List<String> l_name, List<List<String>> l_img){
            this.mContext = mContext;
            this.l_name = l_name;
            this.l_img = l_img;

        }

        @Override
        public int getCount() {
            return l_name.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            View view;
            if(convertView == null){
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(mContext).inflate(R.layout.item_list_sportclass, null);
                view.setTag(viewHolder);
            }else{
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.img = (RoundImageView) view.findViewById(R.id.item_img_three);
            viewHolder.tv = (TextView) view.findViewById(R.id.item_tv_three);
            Glide.with(getContext()).load(l_img.get(i).get(0)).into(viewHolder.img);
            viewHolder.tv.setText(l_name.get(i));
            return view;
        }

        class ViewHolder{
            RoundImageView img;
            TextView tv;
        }
    }
}