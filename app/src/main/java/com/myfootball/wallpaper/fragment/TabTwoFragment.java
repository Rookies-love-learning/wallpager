package com.myfootball.wallpaper.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.activity.SpaceImageDetailActivity;
import com.myfootball.wallpaper.bean.SportBean;
import com.myfootball.wallpaper.util.LocalJsonResolutionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabTwoFragment extends Fragment {

    private View inflate;
    private GridView gridView;
    private GridAdapter myAdapter;
    private String fileName = "sport.json";
    private String imageJson;
    private List<String> list_sport = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(inflate == null){
            inflate = inflater.inflate(R.layout.fragment_tab_two, container, false);
            initData();
            initUI();
            setLister();
        }
        return inflate;
    }

    private void initUI() {
        gridView = (GridView) inflate.findViewById(R.id.grid_two);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        myAdapter = new GridAdapter(getContext(), list_sport);
        gridView.setAdapter(myAdapter);
    }

    private void initData() {
        imageJson = LocalJsonResolutionUtils.getJson(getContext(), fileName);
        SportBean sportBean = LocalJsonResolutionUtils.JsonToObject(imageJson, SportBean.class);
        for (int i = 0; i < sportBean.getData().size(); i++){
            list_sport.add(sportBean.getData().get(i).getUrlImage());
        }
        Collections.reverse(list_sport);
    }

    private void setLister() {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), SpaceImageDetailActivity.class);
                intent.putExtra("img_url", list_sport.get(position));
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    public class GridAdapter extends BaseAdapter{

        private Context mContext;
        private List<String> data;

        public GridAdapter(Context mContext, List<String> data){
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            View view;
            if(convertView == null){
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(mContext).inflate(R.layout.item_list_sport, null);
                view.setTag(viewHolder);
            }else{
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.img = (ImageView) view.findViewById(R.id.item_img_one);
            Glide.with(getContext()).load(data.get(position)).into(viewHolder.img);
            return view;
        }

        class ViewHolder{
            ImageView img;
        }
    }
}