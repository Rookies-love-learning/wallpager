package com.myfootball.wallpaper.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myfootball.wallpaper.R;
import com.myfootball.wallpaper.activity.AboutActivity;
import com.myfootball.wallpaper.activity.FeedbackActivity;
import com.myfootball.wallpaper.activity.SpaceImageDetailActivity;
import com.myfootball.wallpaper.adapter.HomeAdapter;
import com.myfootball.wallpaper.bean.SportBean;
import com.myfootball.wallpaper.util.LocalJsonResolutionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabFourFragment extends Fragment {

    private View inflate;
    private LinearLayout lin_clear;
    private LinearLayout lin_feedback;
    private LinearLayout lin_about;
    private LinearLayout lin_version;

    private String fileName = "sport_part2.json";
    private String imageJson;
    private List<String> list_sport = new ArrayList<>();
    private List<String> stringList = new ArrayList<>();
    private GridView list_mine;
    private ListAdapter listAdapter;

    private HomeAdapter homeAdapter;
    private RecyclerView recyclerView;

    private Dialog progressDialog;

    private int ints[] = {R.mipmap.set_baseketball, R.mipmap.set_jiewu, R.mipmap.set_quanji};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (inflate == null) {
            inflate = inflater.inflate(R.layout.fragment_tab_four, container, false);
            getData();
            initUI();
            setLister();
        }
        return inflate;
    }


    private void initUI() {

//        lin_clear = (LinearLayout) inflate.findViewById(R.id.lin_clear);
//        lin_feedback = (LinearLayout) inflate.findViewById(R.id.lin_feedback);
//        lin_about = (LinearLayout) inflate.findViewById(R.id.lin_about);
//        lin_version = (LinearLayout) inflate.findViewById(R.id.lin_version);

//        list_mine = (GridView) inflate.findViewById(R.id.list_mine);
        list_mine.setSelector(new ColorDrawable(Color.TRANSPARENT));
//        listAdapter = new ListAdapter(getContext(), list_sport);
        //        list_mine.setAdapter(listAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView = inflate.findViewById(R.id.home_recycleview);
        homeAdapter = new HomeAdapter(R.layout.fragment_tab_four);
        recyclerView.setAdapter(homeAdapter);


    }

    private void getData() {
        imageJson = LocalJsonResolutionUtils.getJson(getContext(), fileName);
        SportBean sportBean = LocalJsonResolutionUtils.JsonToObject(imageJson, SportBean.class);
        for (int i = 0; i < sportBean.getData().size(); i++) {
            list_sport.add(sportBean.getData().get(i).getUrlImage());
        }
        Collections.reverse(list_sport);

    }

    private void setLister() {
        lin_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog("缓存清除成功!");

//                Handler handler2 = new Handler();
//                handler2.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast toast1 = new Toast(getActivity());
//                        LayoutInflater l_inflater1 = LayoutInflater.from(getContext());
//                        View view1 = l_inflater1.inflate(R.layout.toast, null);
//                        TextView textView1 = view1.findViewById(R.id.tv_toast);
//                        textView1.setText("缓存清除成功!");
//                        toast1.setView(view1);
//                        toast1.setGravity(Gravity.CENTER, 0, 0);
//                        toast1.setDuration(Toast.LENGTH_SHORT);
//                        toast1.show();
//                    }
//                }, 500);
            }
        });

        lin_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getContext(), FeedbackActivity.class);
                startActivity(intent1);
            }
        });

        lin_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(getContext(), AboutActivity.class);
                startActivity(intent2);
            }
        });

        lin_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog("已经是最新版本了!");


//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast toast2 = new Toast(getActivity());
//                        LayoutInflater l_inflater2 = LayoutInflater.from(getContext());
//                        View view2 = l_inflater2.inflate(R.layout.toast, null);
//                        TextView textView2 = view2.findViewById(R.id.tv_toast);
//                        textView2.setText("已经是最新版本了!");
//                        toast2.setView(view2);
//                        toast2.setGravity(Gravity.CENTER, 0, 0);
//                        toast2.setDuration(Toast.LENGTH_SHORT);
//                        toast2.show();
//                    }
//                }, 500);
            }
        });

        list_mine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), SpaceImageDetailActivity.class);
                intent.putExtra("img_url", list_sport.get(position));
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    public class ListAdapter extends BaseAdapter {

        private Context mContext;
        private List<String> data;

        public ListAdapter(Context mContext, List<String> data) {
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            View view;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(mContext).inflate(R.layout.item_grid_sort, null);
                view.setTag(viewHolder);
            } else {
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.img = (ImageView) view.findViewById(R.id.item_img_one);
            Glide.with(getContext()).load(data.get(position)).into(viewHolder.img);
            return view;
        }

        class ViewHolder {
            ImageView img;
        }
    }

    private void showDialog(String text) {
        progressDialog = new Dialog(getContext(), R.style.progress_dialog);
        progressDialog.setContentView(R.layout.dialog);
        progressDialog.setCancelable(true);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView msg = (TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
        msg.setText("loading...");
        progressDialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    Toast toast = new Toast(getActivity());
                    LayoutInflater l_inflater = LayoutInflater.from(getContext());
                    View view = l_inflater.inflate(R.layout.toast, null);
                    TextView textView = view.findViewById(R.id.tv_toast);
                    textView.setText("" + text);
                    toast.setView(view);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        }, 500);
    }
}