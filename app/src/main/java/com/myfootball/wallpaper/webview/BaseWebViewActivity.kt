package com.down.ny.webview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import androidx.fragment.app.FragmentManager
import androidx.viewbinding.ViewBinding
import com.tencent.smtt.sdk.WebView
import com.myfootball.wallpaper.activity.BaseActivity
import com.myfootball.wallpaper.R
import com.myfootball.wallpaper.databinding.BaseWebviewLayoutBinding

@SuppressLint("JavascriptInterface", "SetJavaScriptEnabled", "NewApi", "Registered")
class BaseWebViewActivity : BaseActivity<BaseWebviewLayoutBinding>() {

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun _initView(savedInstanceState: Bundle?) {
//        setTitle()
        showLoading()
        val urlString = intent.getStringExtra("url")
        urlString?.let {
            initFragment(it)
        }
    }

    override fun _initData() {

    }

    lateinit var commonWebViewFragment: CommonWebViewFragment

    @SuppressLint("CommitTransaction")
    fun initFragment(urlString: String) {
        val mFragmentManager: FragmentManager = supportFragmentManager
        commonWebViewFragment = CommonWebViewFragment.newInstant(urlString)
        val myWebViewCallback = object : MyWebViewCallback {
            override fun progress(view: WebView?, process: Int) {
                if (process == 100) {
                    dismissLoading()
                }
            }

            override fun onReceivedTitle(view: WebView?, title: String?) {
                setTitle(title)
            }
        }
        commonWebViewFragment.setMyWebViewCallback(myWebViewCallback)

        mFragmentManager.beginTransaction()
            .add(R.id.frameLayout, commonWebViewFragment, CommonWebViewFragment.TAG)
            .commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        val result = commonWebViewFragment.onKeyDown(keyCode, event)
        if (result) {
            return true
        } else {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                finish()
                return true
            }
            return super.onKeyDown(keyCode, event)
        }
    }


    companion object {
        fun startActivity(context: Context, url: String) {
            val intent = Intent(context, BaseWebViewActivity::class.java)
            intent.putExtra("url", url)
            context.startActivity(intent)
        }
    }

    override fun getViewBinding(): ViewBinding {
        return BaseWebviewLayoutBinding.inflate(layoutInflater)
    }
}
