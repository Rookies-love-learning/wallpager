package com.myfootball.wallpaper.webview;

import android.content.Context;
import android.os.Handler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {

    public static final int LOADSUCCESS = 1011;
    public static final int LOADFAILED = 1012;
    public static final int LOADTITLE = 1013;
    private Handler mHandler;
    private Context mContext;
    private String TAG = "MyWebViewClient";

    public MyWebViewClient(Context context, Handler handler) {
        // TODO Auto-generated constructor stub
        mContext = context;
        mHandler = handler;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        mHandler.sendEmptyMessage(LOADSUCCESS);
//		view.loadUrl("javascript:(function() { var videos = document.getElementsByTagName('video'); for(var i=0;i<videos.length;i++){videos[i].play();}})()");
        super.onPageFinished(view, url);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode,
                                String description, String failingUrl) {
        // TODO Auto-generated method stub
        mHandler.sendEmptyMessage(LOADFAILED);
        String errorHtml = "<html><body><h1>Page not find!</h1></body></html>";
        view.loadData(errorHtml,
                "text/html", "UTF-8");
        super.onReceivedError(view, errorCode, description, failingUrl);
    }
}
