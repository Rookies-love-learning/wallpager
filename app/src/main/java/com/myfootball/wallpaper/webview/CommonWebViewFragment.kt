package com.down.ny.webview


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tencent.smtt.export.external.interfaces.ConsoleMessage
import com.tencent.smtt.sdk.WebChromeClient
import com.tencent.smtt.sdk.WebView
import com.myfootball.wallpaper.R
import com.myfootball.wallpaper.webview.X5WebView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CommonWebViewFragment : Fragment() {

    private lateinit var mWebView: X5WebView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_common_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData(view)
    }

    private var mMyWebViewCallback: MyWebViewCallback? = null
    public fun setMyWebViewCallback(webViewCallback: MyWebViewCallback) {
        mMyWebViewCallback = webViewCallback
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initData(view: View) {
        val url = arguments?.getString("url")
        mWebView = view.findViewById<View>(R.id.webView) as X5WebView

        mWebView.settings.javaScriptEnabled = true
        mWebView.settings.domStorageEnabled = true
        mWebView.settings.setAppCacheMaxSize((1024 * 1024 * 8).toLong())
        val appCachePath = activity?.cacheDir
            ?.absolutePath
        mWebView.settings.setAppCachePath(appCachePath)
        mWebView.settings.allowFileAccess = true
        mWebView.settings.setAppCacheEnabled(true)
        mWebView.settings.javaScriptCanOpenWindowsAutomatically = true
        mWebView.loadUrl(url)

        mWebView.webChromeClient = object : WebChromeClient() {

            override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
                if (consoleMessage != null) {
                    Log.d(
                        "WEBVIEW",
                        consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId()
                    )
                }
                return super.onConsoleMessage(consoleMessage)
            }

            override fun onReceivedTitle(view: WebView?, title: String?) {
                super.onReceivedTitle(view, title)
                mMyWebViewCallback?.onReceivedTitle(view, title)
            }

            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                if (mMyWebViewCallback != null) {
                    mMyWebViewCallback?.progress(view, newProgress)
                }

            }

        }

    }

    fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (event != null) {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
                return if (mWebView.canGoBack()) {
                    mWebView.goBack()
                    true
                } else {
                    false
                }
            }
        }
        return false
    }


    companion object {
        val TAG = "CommonWebViewFragment"
        fun newInstant(url: String): CommonWebViewFragment {
            val commonWebViewFragment = CommonWebViewFragment()
            val bundle = Bundle()
            bundle.putString("url", url)
            commonWebViewFragment.arguments = bundle
            return commonWebViewFragment
        }
    }

}
