package com.down.ny.webview

import com.tencent.smtt.sdk.WebView

interface MyWebViewCallback {
    fun progress(view: WebView?, process: Int)
    fun onReceivedTitle(view: WebView?, title: String?)
}